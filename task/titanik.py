import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    x=df[df['Name'].str.contains("Mr.")['Age'].isna().sum()
    y=df.where(df['Name'].str.contains("Mr."))['Age'].median()
    y=int(y)
    k=df[df['Name'].str.contains("Mrs.")]['Age'].isna().sum()
    m=df.where(df['Name'].str.contains("Mrs."))['Age'].median()
    m=int(m)
    l=df[df['Name'].str.contains("Miss.")]['Age'].isna().sum()
    n=df.where(df['Name'].str.contains("Miss."))['Age'].median()
    n=int(n)
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    return [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
